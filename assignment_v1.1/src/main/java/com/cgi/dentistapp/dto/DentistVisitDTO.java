package com.cgi.dentistapp.dto;

import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.entity.ServiceTypeEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.sql.Time;

public class DentistVisitDTO {

    public Long id;
    public String date;
    public String time;
    public String clientName;

    public DentistEntity dentist;

    public ServiceTypeEntity service;


    public DentistVisitDTO() {
    }

    public DentistVisitDTO(Long id, DentistEntity dentist,String clientName, ServiceTypeEntity service, String date, String time) {
    this.id = id;
    this.date = date;
    this.time = time;
    this.dentist = dentist;
    this.service = service;
    this.clientName = clientName;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public DentistEntity getDentist() {
        return dentist;
    }

    public void setDentist(DentistEntity dentist) {
        this.dentist = dentist;
    }

    public ServiceTypeEntity getService() {
        return service;
    }

    public void setService(ServiceTypeEntity service) {
        this.service = service;
    }
}
