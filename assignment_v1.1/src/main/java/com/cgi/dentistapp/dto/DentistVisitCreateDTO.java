package com.cgi.dentistapp.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.sql.Time;

public class DentistVisitCreateDTO {

    Long id;
    @NotNull(message = "Arst peab olema valitud!")
    Long dentistId;
    @NotNull(message = "Teenus peab olema valitud!")
    Long serviceId;

    @NotNull(message = "Kuupäev peab olema valitud!")
    java.sql.Date visitDate;

    @NotNull(message = "Kellaaeg peab olema valitud!")
    String visitTime;

    @Size(min = 4, message = "Nimi peab olema vähemalt 4 sümbolit pikk")
    String clientName;

    public DentistVisitCreateDTO() {
    }

    public DentistVisitCreateDTO(Long id, Long dentistId, Long serviceId, java.sql.Date visitDate, String visitTime, String clientName) {
        this.id = id;
        this.dentistId = dentistId;
        this.serviceId = serviceId;
        this.visitDate = visitDate;
        this.visitTime = visitTime;
        this.clientName = clientName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDentistId() {
        return dentistId;
    }

    public void setDentistId(Long dentistName) {
        this.dentistId = dentistName;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(String visitTime) {
        this.visitTime = visitTime;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
