package com.cgi.dentistapp.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class DentistDTO {
    public long id;

    public String name;


    public DentistDTO() {
    }

    public DentistDTO(Long id, String name) {
        this.id = id;
        this.name = name;

    }

}
