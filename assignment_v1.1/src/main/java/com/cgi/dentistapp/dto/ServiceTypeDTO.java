package com.cgi.dentistapp.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class ServiceTypeDTO {
    public long id;

    public String typeName;

    public ServiceTypeDTO() {
    }

    public ServiceTypeDTO(Long id,String typeName) {
        this.id = id;
        this.typeName = typeName;
    }

}
