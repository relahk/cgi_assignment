package com.cgi.dentistapp.dto;

public class DeleteDTO {
    public Long id;

    public DeleteDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}


