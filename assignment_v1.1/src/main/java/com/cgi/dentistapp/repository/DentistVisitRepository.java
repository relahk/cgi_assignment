package com.cgi.dentistapp.repository;

import com.cgi.dentistapp.entity.ServiceTypeEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.cgi.dentistapp.entity.DentistVisitEntity;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface DentistVisitRepository extends CrudRepository<DentistVisitEntity,Long> {

    @Query("FROM DentistVisitEntity WHERE id =:id")
    DentistVisitEntity findById(@Param("id") Long id);

    @Query("FROM DentistVisitEntity order by date,time,dentist.id")
    List<DentistVisitEntity> getAll();

    @Query("FROM DentistVisitEntity WHERE dentist.id =:dentist and date =:date")
    List<DentistVisitEntity> getDentistVisitEntityByDentistAndDate(@Param("dentist")Long dentist,@Param("date") java.sql.Date date);

    //Queries for filtering

}
