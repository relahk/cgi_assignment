package com.cgi.dentistapp.repository;

import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.entity.ServiceTypeEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.cgi.dentistapp.entity.DentistVisitEntity;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface ServiceTypeRepository extends CrudRepository<ServiceTypeEntity,Long> {

    @Query("FROM ServiceTypeEntity WHERE id =:id")
    ServiceTypeEntity findById(@Param("id") Long id);


    @Override
    @Query("FROM ServiceTypeEntity ")
    List<ServiceTypeEntity> findAll();
}
