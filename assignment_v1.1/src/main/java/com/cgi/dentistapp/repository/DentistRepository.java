package com.cgi.dentistapp.repository;

import com.cgi.dentistapp.entity.ServiceTypeEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.cgi.dentistapp.entity.DentistEntity;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DentistRepository extends CrudRepository<DentistEntity,Long> {

    @Override
    @Query("FROM DentistEntity ")
    List<DentistEntity> findAll();

    @Query("FROM DentistEntity where id =:id")
    DentistEntity getDentistById(@Param("id") Long id);
}
