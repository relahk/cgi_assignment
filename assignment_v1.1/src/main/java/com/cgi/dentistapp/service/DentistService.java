package com.cgi.dentistapp.service;


import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.repository.DentistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class DentistService {

    @Autowired
    DentistRepository repository;

    public List<DentistEntity> getAll(){
        return repository.findAll();
    }

    public DentistEntity getById(Long id){
        return repository.getDentistById(id);
    }
}
