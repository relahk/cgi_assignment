package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.dto.ServiceTypeDTO;
import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.entity.DentistVisitEntity;
import com.cgi.dentistapp.entity.ServiceTypeEntity;
import com.cgi.dentistapp.repository.DentistRepository;
import com.cgi.dentistapp.repository.DentistVisitRepository;
import com.cgi.dentistapp.repository.ServiceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ServiceTypeService {

    @Autowired
    ServiceTypeRepository repository;

    public List<ServiceTypeEntity> getAll(){
        return repository.findAll();
    }

    public ServiceTypeEntity getById(Long id){
        return  repository.findById(id);
    }
    public void update(ServiceTypeEntity entity){
        repository.save(entity);
    }
}
