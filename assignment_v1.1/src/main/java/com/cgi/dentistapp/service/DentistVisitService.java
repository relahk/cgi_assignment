package com.cgi.dentistapp.service;


import com.cgi.dentistapp.dto.DentistVisitCreateDTO;
import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.entity.DentistVisitEntity;
import com.cgi.dentistapp.entity.ServiceTypeEntity;
import com.cgi.dentistapp.repository.DentistRepository;
import com.cgi.dentistapp.repository.DentistVisitRepository;
import com.cgi.dentistapp.repository.ServiceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    DentistVisitRepository repository;
    @Autowired
    private DentistRepository dentistRepository;
    @Autowired
    private ServiceTypeRepository serviceTypeRepository;

    //List of appointment times that said dentist has available that date
    public List<String> availableTimes(Long dentistId,java.sql.Date date ){
        List<String> times = getAllTimes();
        List<DentistVisitEntity> entityList  = repository.getDentistVisitEntityByDentistAndDate(dentistId,date);
        for (DentistVisitEntity entity:entityList)
        {

         times.remove(entity.getTime());
        }

        return times;
    }

    public List<DentistVisitEntity> getAll(){

        return repository.getAll();
    }
    public DentistVisitEntity getById(Long id){
        return repository.findById(id);
    }

    public void add(DentistVisitCreateDTO newVisit){
        DentistEntity dentist = dentistRepository.findOne(newVisit.getDentistId());
        ServiceTypeEntity service = serviceTypeRepository.findById(newVisit.getServiceId());

        DentistVisitEntity entity = new DentistVisitEntity();

        entity.setClientName(newVisit.getClientName());
        entity.setDate(newVisit.getVisitDate());
        entity.setTime(newVisit.getVisitTime());
        entity.setDentist(dentist);
        entity.setService(service);


        repository.save(entity);
    }
    public void update(DentistVisitCreateDTO updateVisit){
        DentistEntity dentist = dentistRepository.findOne(updateVisit.getDentistId());
        ServiceTypeEntity service = serviceTypeRepository.findById(updateVisit.getServiceId());

        DentistVisitEntity entity = new DentistVisitEntity();

        entity.setId(updateVisit.getId());
        entity.setClientName(updateVisit.getClientName());
        entity.setDate(updateVisit.getVisitDate());
        entity.setTime(updateVisit.getVisitTime());
        entity.setDentist(dentist);
        entity.setService(service);

        repository.save(entity);
    }
    public void delete(Long id){
        repository.delete(id);
    }
    //List of all appointment times that a dentist can have on one day
   public List<String> getAllTimes(){
        List<String> times = new ArrayList<>();
       times.add("08:00");
       times.add("09:00");
       times.add("10:00");
       times.add("11:00");
       times.add("12:00");
       times.add("13:00");
       times.add("14:00");
       times.add("15:00");
       times.add("16:00");
       times.add("17:00");
       times.add("18:00");
    return times;
   }



}
