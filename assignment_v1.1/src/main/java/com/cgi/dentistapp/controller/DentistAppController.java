package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dto.*;
import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.entity.DentistVisitEntity;
import com.cgi.dentistapp.entity.ServiceTypeEntity;
import com.cgi.dentistapp.service.DentistService;
import com.cgi.dentistapp.service.DentistVisitService;
import com.cgi.dentistapp.service.ServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistService dentistService;
    @Autowired
    private DentistVisitService dentistVisitService;
    @Autowired
    private ServiceTypeService serviceTypeService;


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    @GetMapping("/")
    public String index(){

        return "index";
    }
    //page for making new appointment
    @GetMapping("/newAppointment")
    public String showRegisterForm(DentistVisitCreateDTO dentistVisitCreateDTO, Model model) {
        List<String> allTimes = dentistVisitService.getAllTimes();
        List<DentistEntity> dentists = dentistService.getAll();
        List<ServiceTypeEntity> services = serviceTypeService.getAll();
        model.addAttribute("dentists",dentists);
        model.addAttribute("services",services);
        model.addAttribute("allTimes",allTimes);



        return "form";
    }

    @PostMapping("/newAppointment")
    public String postRegisterForm(@Valid DentistVisitCreateDTO dentistVisitCreateDTO, BindingResult bindingResult,Model model) {

        List<String> availableTimes = dentistVisitService.availableTimes(dentistVisitCreateDTO.getDentistId(),dentistVisitCreateDTO.getVisitDate());
        DentistEntity dentist = dentistService.getById(dentistVisitCreateDTO.getDentistId());

        if (bindingResult.hasErrors() || !availableTimes.contains(dentistVisitCreateDTO.getVisitTime())) {
            if(!availableTimes.contains(dentistVisitCreateDTO.getVisitTime())){
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                String formatedDate = sdf.format(dentistVisitCreateDTO.getVisitDate());
                bindingResult.addError(new FieldError("dentistVisitCreateDTO","visitTime",("Arstil " + dentist.getName() + " on kuupäeval " +formatedDate + " kell " + dentistVisitCreateDTO.getVisitTime() + " visiit juba registreeritud" )));
            }

            List<String> allTimes = dentistVisitService.getAllTimes();
            List<DentistEntity> dentists = dentistService.getAll();
            List<ServiceTypeEntity> services = serviceTypeService.getAll();

            model.addAttribute("dentists",dentists);
            model.addAttribute("services",services);
            model.addAttribute("allTimes",allTimes);
            return "form";
        }

        dentistVisitService.add(dentistVisitCreateDTO);
        return "redirect:/results";
    }
    // page with all appointments
    @GetMapping("/appointmentList")
    public String appointments(@RequestParam(required = false) Long dentistId,@RequestParam(required = false) Long serviceId,@RequestParam(required = false) java.sql.Date startDate,@RequestParam(required = false) java.sql.Date endDate,DentistVisitCreateDTO dentistVisitCreateDTO,Model model){
        SimpleDateFormat sdf = new SimpleDateFormat(
                "dd.MM.yyyy");
        List<DentistVisitEntity> appointments = dentistVisitService.getAll();
        List<DentistVisitDTO> outbound = new ArrayList<>();
        appointments.forEach(entity ->{
            if((dentistId == null || dentistId == entity.getDentist().getId()) && (serviceId == null || serviceId == entity.getService().getId()) && (startDate == null || startDate.before(entity.getDate()) || startDate.equals(entity.getDate()))&& (endDate == null || endDate.after(entity.getDate()) || endDate.equals(entity.getDate()))){

                outbound.add(new DentistVisitDTO(entity.getId(),entity.getDentist(),entity.getClientName(),entity.getService(),sdf.format(entity.getDate()),entity.getTime()));
            }
        });
        List<ServiceTypeEntity> services = serviceTypeService.getAll();
        List<DentistEntity> dentists = dentistService.getAll();
        model.addAttribute("appointments",outbound);
        model.addAttribute("dentists",dentists);
        model.addAttribute("services",services);
        return "/appointmentList";
    }
    // page for editing an appointment
    @GetMapping("/appointment")
    public  String appointment(@RequestParam String id, DentistVisitCreateDTO dentistVisitCreateDTO, Model model){
        DentistVisitEntity appointment = dentistVisitService.getById(Long.parseLong(id));
        if(appointment != null){
            List<String> allTimes = dentistVisitService.getAllTimes();
            List<DentistEntity> dentists = dentistService.getAll();
            List<ServiceTypeEntity> services = serviceTypeService.getAll();

            model.addAttribute("dentists",dentists);
            model.addAttribute("services",services);
            model.addAttribute("appointment",appointment);
            model.addAttribute("allTimes",allTimes);
            return "/appointment";
        }
        return "/appointmentList";
    }

    @PostMapping("/appointment")
    public String appointmentPost(@Valid DentistVisitCreateDTO dentistVisitCreateDTO,BindingResult bindingResult, Model model){

        List<String> availableTimes = dentistVisitService.availableTimes(dentistVisitCreateDTO.getDentistId(),dentistVisitCreateDTO.getVisitDate());
        DentistVisitEntity appointment = dentistVisitService.getById(dentistVisitCreateDTO.getId());
        DentistEntity dentist = dentistService.getById(dentistVisitCreateDTO.getDentistId());

        availableTimes.add(appointment.getTime());
        if (bindingResult.hasErrors() || !availableTimes.contains(dentistVisitCreateDTO.getVisitTime())) {
            if(!availableTimes.contains(dentistVisitCreateDTO.getVisitTime())){
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                String formatedDate = sdf.format(dentistVisitCreateDTO.getVisitDate());
                bindingResult.addError(new FieldError("dentistVisitCreateDTO","visitTime",("Arstil " + dentist.getName() + " on kuupäeval " +formatedDate + " kell " + dentistVisitCreateDTO.getVisitTime() + " visiit juba registreeritud" )));
            }

            List<String> allTimes = dentistVisitService.getAllTimes();
            List<DentistEntity> dentists = dentistService.getAll();
            List<ServiceTypeEntity> services = serviceTypeService.getAll();

            model.addAttribute("dentists",dentists);
            model.addAttribute("appointment",appointment);
            model.addAttribute("services",services);
            model.addAttribute("allTimes",allTimes);
            return "appointment";
        }

        dentistVisitService.update(dentistVisitCreateDTO);
        return "redirect:/appointmentList";
    }

    @PostMapping("/appointment/delete")
    public String appointmentDelete(DeleteDTO deleteDTO){

        if(deleteDTO.id != null){
            dentistVisitService.delete(deleteDTO.id);
        }
        return "redirect:/appointmentList";
    }


}
