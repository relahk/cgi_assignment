-- noinspection SqlNoDataSourceInspectionForFile

insert into dentist(name) values('Erik Maldonado');
insert into dentist(name) values('Macauly Robinson');

insert into service_type(type_name) values('Hambaravi');
insert into service_type(type_name) values('Hambaravi konsultatsioon');
insert into service_type(type_name) values('Hamba eemaldamine');
insert into service_type(type_name) values('Igeme ravi');

insert into dentist_visit(client_name,dentist_id,service_id,time,date) values('Jimmi Pilsner',1,2,'13:00',PARSEDATETIME('21.01.2021','dd.MM.yyyy'));
insert into dentist_visit(client_name,dentist_id,service_id,time,date) values('Jimmi Pilsner',2,1,'14:00',PARSEDATETIME('12.11.2021','dd.MM.yyyy'));
insert into dentist_visit(client_name,dentist_id,service_id,time,date) values('Jimmi Pilsner',1,2,'11:00',PARSEDATETIME('18.05.2021','dd.MM.yyyy'));
insert into dentist_visit(client_name,dentist_id,service_id,time,date) values('Jimmi Pilsner',1,1,'11:00',PARSEDATETIME('28.04.2021','dd.MM.yyyy'));
insert into dentist_visit(client_name,dentist_id,service_id,time,date) values('Jimmi Pilsner',1,3,'12:00',PARSEDATETIME('28.04.2021','dd.MM.yyyy'));

